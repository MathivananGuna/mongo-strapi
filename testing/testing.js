var MongoClient = require('mongodb').MongoClient;
const urlFile = require('../common/configure');
const mongoose = require('mongoose');
const mongoDbURL = urlFile.databaseURL;
const loginCredential = urlFile.loginDatabaseName;
const loginCollectionName = urlFile.loginCollectionName;
const Schema = mongoose.Schema;
module.exports = (app) => {
    app.route('/testing')
        .get(getTesting)
        .post(postTesting)
}

getTesting = async (req, res) => { }
postTesting = async (req, res) => { }