const mongoose = require('mongoose');
const urlFile = require('../common/configure');
const schemaFile = require('./notification-schema');
const mongoDbURL = urlFile.databaseURL;
const pubnubKeyData = urlFile.pubnubSecretKey;
const PubNub = require('pubnub');
const createUserDb = urlFile.createUserDbName;
const createUserCol = urlFile.createUserColName;
const notificationCol = urlFile.notificationColName;

// database connection section
const connectuserDb = mongoose.createConnection(`${mongoDbURL}/${createUserDb}`, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });

// Schema section
const notificationSchema = schemaFile.notificationSchema
const userSchema = schemaFile.userSchema

// collection connection
const connectUser = connectuserDb.model(`${createUserCol}`, userSchema);

module.exports = (app) => {
    app.route('/notifications')
        .get(getAllNotifications)
        .post(postNewNotifications)
        .put(changeNotifyStatus)
}

getAllNotifications = (req, res) => { // get 
    try {
        const connectNotificationDb = mongoose.createConnection(`${mongoDbURL}/${req.query.orgId}`, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
        const connectNotifiyCol = connectNotificationDb.model(`${notificationCol}`, userSchema);
        let noOfPendingView = 0; let noOfReadStatus = 0;
        connectNotifiyCol.find(async (err, results) => {
            let sendResults = results.filter((x) => { return x._doc.clearStatus === false })
            if (results.length === 0) {
                res.send({
                    status: "success", message: "success",
                    data: { orgId: req.body.orgId, result: sendResults.reverse(), viewStatus: noOfPendingView, readStatus: noOfReadStatus }
                });
            }
            else {
                results.forEach((dataOne, i) => {
                    if (dataOne._doc.viewStatus === false) { noOfPendingView = noOfPendingView + 1 }
                    if (dataOne._doc.readStatus === false) { noOfReadStatus = noOfReadStatus + 1 }
                    if (results.length - 1 === i) {
                        res.send({
                            status: "success", message: "success",
                            data: { orgId: req.body.orgId, result: sendResults.reverse(), viewStatus: noOfPendingView, readStatus: noOfReadStatus }
                        });
                    }
                })
            }
        })
    }
    catch (err) {
        res.send({
            status: "failed", message: "failed",
            data: { orgId: req.query.orgId, result: [] }
        });
    }
    finally { }
}

postNewNotifications = (req, res) => { // post
    // try {
    //     var reqRoleSmall = req.body.role.map(function (x) { return x.toLowerCase(); })
    //     connectUser.find((err, results) => {
    //         results.forEach((data) => {
    //             let resultRoleSmall = data._doc.role.toLowerCase()
    //             if (reqRoleSmall.includes(resultRoleSmall) === true || reqRoleSmall.includes('all') === true) {
    //                 const connectNotificationDb = mongoose.createConnection(`${mongoDbURL}/${req.body.userId}`, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
    //                 const connectNotification = connectNotificationDb.model(`${notificationCol}`, notificationSchema);
    //                 d = new Date();
    //                 utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    //                 nd = new Date(utc + (3600000 * +5.5));
    //                 req.body.createdAt = nd;
    //                 let createObject = new connectNotification(req.body);
    //                 createObject.save();
    //                 const pubnub = new PubNub(pubnubKeyData);
    //                 const pubnubResult = pubnub.publish({
    //                     channel: [`${req.body.userId}`],
    //                     message: {
    //                         title: "new message",
    //                         description: createObject
    //                     },
    //                 });
    //             }
    //             else { }
    //         })
    //         res.send({
    //             status: "success", message: "success",
    //             data: req.body
    //         });
    //     })
    // }
    // catch (err) {
    //     res.send({
    //         status: "failed", message: "failed",
    //         data: req.body, error: err
    //     });
    // }
    // finally { }

    try {
        const connectNotificationDb = mongoose.createConnection(`${mongoDbURL}/${req.body.orgId}`, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
        const connectNotification = connectNotificationDb.model(`${notificationCol}`, notificationSchema);
        d = new Date();
        utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        nd = new Date(utc + (3600000 * +5.5));
        req.body.createdAt = nd;
        let createObject = new connectNotification(req.body);
        createObject.save();
        const pubnub = new PubNub(pubnubKeyData);
        const pubnubResult = pubnub.publish({
            channel: [`${req.body.orgId}`],
            message: {
                title: "new message",
                description: createObject
            },
        });
        res.send({
            status: "success", message: "success",
            data: req.body
        });
    }
    catch (err) {
        res.send({
            status: "failed", message: "failed",
            data: req.body, error: err
        });
    }
    finally {

    }
}

changeNotifyStatus = (req, res) => {
    if (req.body.type !== undefined || req.body.type !== "" && req.body.orgId !== undefined || req.body.orgId !== "") {
        const connectNotificationDb = mongoose.createConnection(`${mongoDbURL}/${req.body.orgId}`, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
        if (req.body.type.toLowerCase() === "viewall") {
            try {
                const connectNotifiyCol = connectNotificationDb.model(`${notificationCol}`, { viewStatus: { type: Boolean } });
                connectNotifiyCol.updateMany({ viewStatus: true }, (err, results) => {
                    if (err) {
                        res.send({
                            status: "failed", message: "failed to update",
                            data: { err: err }
                        });
                    }
                    else {
                        res.send({
                            status: "success", message: "successfully updated",
                            data: { results: results }
                        });
                    }
                })
            }
            catch (err) {
                res.send({
                    status: "failed", message: "functional error",
                    data: { err: err }
                });
            }
            finally { }
        }
        else if (req.body.type.toLowerCase() === "readall") {
            try {
                const connectNotifiyCol = connectNotificationDb.model(`${notificationCol}`, { readStatus: { type: Boolean } });
                connectNotifiyCol.updateMany({ readStatus: true }, (err, results) => {
                    if (err) {
                        res.send({
                            status: "failed", message: "failed to update",
                            data: { err: err }
                        });
                    }
                    else {
                        res.send({
                            status: "success", message: "successfully updated",
                            data: { results: results }
                        });
                    }
                })
            }
            catch (err) {
                res.send({
                    status: "failed", message: "functional error",
                    data: { err: err }
                });
            }
            finally { }
        }
        if (req.body.type.toLowerCase() === "readone") {
            try {
                const connectNotifiyCol = connectNotificationDb.model(`${notificationCol}`, { readStatus: { type: Boolean } });
                connectNotifiyCol.findByIdAndUpdate(mongoose.Types.ObjectId(req.body.notifyId), { $set: { readStatus: true } }, { new: true }, (err, results) => {
                    if (err) {
                        res.send({
                            status: "failed", message: "failed to update",
                            data: { err: err }
                        });
                    }
                    else {
                        res.send({
                            status: "success", message: "successfully updated",
                            data: { results: results }
                        });
                    }
                })
            }
            catch (err) {
                res.send({
                    status: "failed", message: "functional error",
                    data: { err: err }
                });
            }
            finally { }
        }
        else if (req.body.type.toLowerCase() === "clearall") {
            try {
                const connectNotifiyCol = connectNotificationDb.model(`${notificationCol}`, { clearStatus: { type: Boolean } });
                connectNotifiyCol.updateMany({ clearStatus: true }, (err, results) => {
                    if (err) {
                        res.send({
                            status: "failed", message: "failed to update",
                            data: { err: err }
                        });
                    }
                    else {
                        res.send({
                            status: "success", message: "successfully updated",
                            data: { results: results }
                        });
                    }
                })
            }
            catch (err) {
                res.send({
                    status: "failed", message: "functional error",
                    data: { err: err }
                });
            }
            finally { }
        }
    }
    else {
        res.send({
            status: "failed", message: "Please provide type of operations to be handled",
            data: {}
        });
    }
}

// DOCUMENTATION: NOTIFICATIONS

// URL: http://localhost:5000/notifications?orgId=603e3eb3de86b431205bfd9e 
// METHOD: GET
// orgId --> Check createUser API DOC
// MongoDB--> DB-notification_user_list COL-user_lists
// Flow: create user and create new notifications and check 



// URL: http://localhost:5000/notifications
// METHOD: POST
// PAYLOAD: {
//     "userId": "",
//     "senderEmail": "",
//     "senderName": "",
//     "message": "",
//     "role": [
//         ""
//     ]
// }
// Flow: create user and post this api and check message by using pubnub.html (support_file)



// URL: http://localhost:5000/notifications
// METHOD: PUT
// PAYLOAD:{
    // type:"",
    // data:[],
    // orgId: ""
// }
// Flow: Call this to update viewStatus:true to false for all objects in collection





