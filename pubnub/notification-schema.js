const mongoose = require('mongoose');
const { ObjectId } = require("mongodb");

const notificationSchema = new mongoose.Schema({
    orgId: {
        type: String,
        required: [true, ""]
    },
    // senderEmail: {
    //     type: String,
    //     required: [true, ""]
    // },
    // senderName: {
    //     type: String,
    //     required: [true, ""],
    // },
    readStatus: {
        type: Boolean,
        required: [false, ""],
        default: false
    },
    viewStatus: {
        type: Boolean,
        required: [false, ""],
        default: false
    },
    clearStatus:{
        type:Boolean,
        required:[true, ""],
        default: false
    },
    status:{
        type:String,
        required:[true, ""],
        default: "success"
    },
    title: {
        type: String,
        required: [true, ""]
    },
    message: {
        type: String,
        required: [true, ""]
    },
    createdAt: {
        type: Date,
        required: [false, ""],
        default: new Date()
    },
    role: {
        type: Array,
        required: [true, ""]
    },
    actionURL:{
        type:String,
        required:[false, ""]
    }
});

const userSchema = mongoose.Schema({
    userId: {
        type: String,
        required: [true, ""]
    },
    userName: {
        type: String,
        required: [true, ""]
    },
    userEmail: {
        type: String,
        required: [true, ""]
    },
    role: {
        type: String,
        required: [true, ""]
    }
})

module.exports = { notificationSchema, userSchema }