const mongoose = require('mongoose');
const loginSchema = new mongoose.Schema(
    {
        userName:{
            type:String,
            required:[true, "please provide valid username"]
        },
        password:{
            type:String,
            required:[true, "please provide valid password"]
        }
    }
)
module.exports = loginSchema;