const mongoose = require('mongoose');
const registerSchema = new mongoose.Schema(
    {
        firstName: {
            type: String,
            required: [true, "first name is required"]
        },
        lastName: {
            type: String,
            required: [true, "last name is required"]
        },
        email: {
            type: String,
            required: [true, "email is required"]
        },
        phoneNumber: {
            type: Number,
            required: [true, "phone number is required"]
        },
        addressOne: {
            type: String,
            required: [false, "address is required"]
        },
        addressTwo: {
            type: String,
            required: [false, "address is required"]
        },
        cityTown: {
            type: String,
            required: [true, "city/town is required"]
        },
        stateRegion: {
            type: String,
            required: [true, "state/region is required"]
        },
        pincode:{
            type:String,
            required:[true, "pincode is required"]
        },
        country:{
            type:String,
            required:[true, "coountry is required"]
        },
        password:{
            type:String,
            required:[true, "password is required"]
        }
    }
)
module.exports = registerSchema;