var MongoClient = require('mongodb').MongoClient;
const mongoose = require('mongoose');
const registerSchema = require('./register-schema');
const loginSchema = require('./login-schema');
const urlFile = require('../common/configure');
const mongoDbURL = urlFile.databaseURL;
const loginCredential = urlFile.loginDatabaseName;
const loginCollectionName = urlFile.loginCollectionName;
const registerDetails = urlFile.registerDetails;

module.exports = (app) => {
    app.route('/register')
        .post(regsiterNewuser)
}
regsiterNewuser = async (req, res) => {
    try {
        var loginPayload = {
            userName: req.body.email,
            password: req.body.password
        }
        MongoClient.connect(`${mongoDbURL}`, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
            var dbo = db.db(`${loginCredential}`);
            var query = { userName: req.body.email };
            dbo.collection(`${loginCollectionName}`).find(query).toArray(function (err, result) {
                if (result.length === 0) {
                    const loginConnection = mongoose
                        .createConnection(`${mongoDbURL}/${loginCredential}`, {
                            useNewUrlParser: true,
                            useCreateIndex: true,
                            useUnifiedTopology: true,
                        })
                    let loginModel = loginConnection.model(`${loginCollectionName}`, loginSchema);
                    let newLoginModel = new loginModel(loginPayload);
                    newLoginModel.save();
                    const registerConnection = mongoose
                        .createConnection(`${mongoDbURL}/${newLoginModel._id}`, {
                            useNewUrlParser: true,
                            useCreateIndex: true,
                            useUnifiedTopology: true,
                        })
                    let registerModel = registerConnection.model(`${registerDetails}`, registerSchema);
                    let newRegister = new registerModel(req.body);
                    newRegister.save();
                    res.send({ status: "success", message: "registered successfully" });
                }
                else {
                    res.send({ status: "failed", message: "email already registered. please try with different email" });
                }
            })
        })
    }
    catch (err) {
        console.log(err);
        res.send({ status: "failed", message: "failed to register. please try again" });
    }
    finally { }
}