const mongoose = require('mongoose');

const loginSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: [true, "please provide email"]
        },
        password: {
            type: String,
            required: [true, "please provide password"]
        }
    }
)

const registerSchema = new mongoose.Schema(
    {
        firstName: {
            type: String,
            required: [true, "please provide first name"]
        },
        email:{
            type:String,
            required:[true, "please provide email"]
        },
        phoneNumber:{
            type:Number,
            required:[true, "please provide phone number"]
        },
        password:{
            type:String,
            required:[true, "Please provide password"]
        }
    }
)

module.exports = { loginSchema, registerSchema }