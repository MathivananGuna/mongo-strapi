var MongoClient = require('mongodb').MongoClient;
const urlFile = require('../common/configure');
const mongoDbURL = urlFile.databaseURL;
const userLoginDbName = urlFile.userRegisterDbName;
const userLoginColName = urlFile.userRegisterColName;
const jwt = require('jsonwebtoken');

module.exports = (app) => {
    app.route('/login/user')
        .post(loginuserData)
}
loginuserData = (req, res) => {
    if (req.body.email === "" || req.body.email === undefined || req.body.email === null) {
        res.send({
            status: "failed", message: "Please provide email",
            data: { email: req.body.email }
        });
    }
    else if (req.body.password === "" || req.body.password === undefined || req.body.password === null) {
        res.send({
            status: "failed", message: "Please provide password",
            data: { email: req.body.email }
        });
    }
    else {
        var emailValidator = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (emailValidator.test(req.body.email) === true) {
            MongoClient.connect(`${mongoDbURL}`, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
                var dbo = db.db(`${userLoginDbName}`);
                dbo.collection(`${userLoginColName}`).find({ email: req.body.email }).toArray(async function (err, result) {
                    if (result.length === 0) {
                        await res.send({
                            status: "failed", message: "entered email is not registered in database. please register",
                            data: { email: req.body.email }
                        });
                    }
                    else {
                        const resultedOutput = result[0];
                        if (resultedOutput.password === req.body.password) {
                            const convertJwt = {
                                id: resultedOutput._id,
                                email: resultedOutput.email,
                                firstName: resultedOutput.firstName
                            }
                            jwt.sign({ ...convertJwt }, 'secretkey', (err, token) => {
                                res.send({
                                    status: "success", message: "successfully logged in",
                                    data: { id: resultedOutput._id, email: resultedOutput.email, firstName: resultedOutput.firstName, phoneNumber: resultedOutput.phoneNumber }, token
                                });
                            })
                        }
                        else {
                            await res.send({
                                status: "failed", message: "entered password is wrong. please check",
                                data: { email: req.body.email }
                            });
                        }
                    }
                })
            })
        }
        else {
            res.send({
                status: "failed", message: `${req.body.email} is not a valid email. please enter valid email address`,
                data: { email: req.body.email }
            });
        }
    }
}

// DOCUMENTATION: LOGIN USER
// URL: http://34.123.166.159:5000/login/user
// METHOD: POST
// PAYLOAD: {
//     "email":"",
//     "password":""
// }
// Flow: register new user and try login 
// Comments: Response with JWT token