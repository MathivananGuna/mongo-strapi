var MongoClient = require('mongodb').MongoClient;
const mongoose = require('mongoose');
const urlFile = require('../common/configure');
const mongoDbURL = urlFile.databaseURL;
const userRegisterDbName = urlFile.userRegisterDbName;
const userRegisterColName = urlFile.userRegisterColName;
const registerSchema = require('./user-schema').registerSchema;

module.exports = (app) => {
    app.route('/register/user')
        .get(getRegisterUserData)
        .post(registerUserData)
}

getRegisterUserData = (req, res) => {
    MongoClient.connect(`${mongoDbURL}`, { useNewUrlParser: true, useUnifiedTopology: true }, async function (err, db) {
        var dbo = db.db(`${userRegisterDbName}`);
        dbo.collection(`${userRegisterColName}`).find({}).toArray(async function (err, result) {
            console.log(result);
            res.json(result);
        })
    })
}

registerUserData = (req, res) => {
    try {
        if (req.body.email === "" || req.body.email === undefined || req.body.email === null) {
            res.send({
                status: "failed", message: "Please provide email",
                data: { email: req.body.email, firstName: req.body.firstName, phoneNumber: req.body.phoneNumber }
            });
        }
        else if (req.body.firstName === "" || req.body.firstName === undefined || req.body.firstName === null) {
            res.send({
                status: "failed", message: "Please provide firstName",
                data: { email: req.body.email, firstName: req.body.firstName, phoneNumber: req.body.phoneNumber }
            });
        }
        else {
            var emailValidator = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (emailValidator.test(req.body.email) === true) {
                MongoClient.connect(`${mongoDbURL}`, { useNewUrlParser: true, useUnifiedTopology: true }, async function (err, db) {
                    var dbo = db.db(`${userRegisterDbName}`);
                    var query = { email: req.body.email };
                    dbo.collection(`${userRegisterColName}`).find(query).toArray(async function (err, result) {
                        if (result.length === 0) {
                            const registerConnection = mongoose
                                .createConnection(`${mongoDbURL}/${userRegisterDbName}`, {
                                    useNewUrlParser: true,
                                    useCreateIndex: true,
                                    useUnifiedTopology: true,
                                })
                            let registerModel = registerConnection.model(`${userRegisterColName}`, registerSchema);
                            let newRegisterModel = new registerModel(req.body);
                            newRegisterModel.save()
                            res.send({
                                status: "success", message: "registered successfully",
                                data: { email: req.body.email, firstName: req.body.firstName, phoneNumber: req.body.phoneNumber }
                            });
                        }
                        else {
                            await res.send({
                                status: "failed", message: "email already registered. please try with different email",
                                data: { email: req.body.email, firstName: req.body.firstName, phoneNumber: req.body.phoneNumber }
                            });
                        }
                    })
                })
            }
            else {
                res.send({
                    status: "failed", message: `${req.body.email} is not a valid email. please enter valid email address`,
                    data: { email: req.body.email, firstName: req.body.firstName, phoneNumber: req.body.phoneNumber }
                });
            }
        }
    }
    catch (err) {
        console.log(err);
        res.send({ status: "failed", message: "database error. please inform admin" });
    }
    finally { }
}


// DOCUMENTATION: REGISTER USER
// URL: http://34.123.166.159:5000/register/user
// METHOD: POST
// PAYLOAD: {
//     "firstName":"",
//     "email":"",
//     "phoneNumber":"",
//     "password":""
// }
// Flow: Register new user and check in login

// URL: http://34.123.166.159:5000/register/user
// MRTHOD: GET
// FLOW: register new user and check list of registered users