var MongoClient = require('mongodb').MongoClient;
const urlFile = require('../common/configure');
const mongoDbURL = urlFile.databaseURL;
const loginCredential = urlFile.loginDatabaseName;
const loginCollectionName = urlFile.loginCollectionName;
const loginSchema = require('../register/login-schema');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

module.exports = (app) => {
    app.route('/login')
        .post(getLoginDetails)
}
getLoginDetails = (req, res) => {
    if (req.body.email !== undefined && req.body.email !== "") {
        if (req.body.password !== undefined && req.body.email !== "") {
            var emailValidator = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (emailValidator.test(req.body.email) === true) {
                var userLogin = {
                    userName: req.body.email
                }
                MongoClient.connect(`${mongoDbURL}`, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
                    var dbo = db.db(`${loginCredential}`);
                    dbo.collection(`${loginCollectionName}`).find(userLogin).toArray(function (err, result) {
                        if (result.length === 0) {
                            res.send({ status: "failed", message: "entered email is not registered in database. please register", data: {} })
                        }
                        else {
                            const resultedOutput = result[0];
                            if (String(req.body.password) === String(resultedOutput.password)) {
                                const convertJwt = {
                                    id: resultedOutput._id,
                                    email: resultedOutput.userName
                                }
                                jwt.sign({ ...convertJwt }, 'secretkey', (err, token) => {
                                    res.json({status:"success", message:"login successful", data:{id:resultedOutput._id}, token })
                                })
                            }
                            else {
                                res.send({ status: "failed", message: "entered password is wrong. please enter valid one", data: {} })
                            }
                        }
                    })
                })
            }
            else { res.send({ status: "failed", message: "enter valid email address", data: {} }) }
        }
        else {
            res.send({ status: "failed", message: "password is empty. please send password", data: {} })
        }
    }
    else {
        res.send({ status: "failed", message: "email is empty. please send email", data: {} })
    }
}

// function verifyToken(req, res, next) {
//     const headerData = req.headers['authorization'];
//     if (typeof headerData !== 'undefined') {
//         const headerSplit = headerData.split(' ');
//         const bearerToken = headerSplit[1];
//         req.token = bearerToken;
//         next();
//     }
//     else {
//         res.sendStatus(403)
//     }
// }