var MongoClient = require('mongodb').MongoClient;
const jwt = require('jsonwebtoken');
const urlFile = require('../common/configure');
const mongoDbURL = urlFile.databaseURL;
const registerDetails = urlFile.registerDetails;

module.exports = (app) => {
    app.route('/getDetails')
        .get(getUserDetails)
}

getUserDetails = (req, res) => {
    let userDbName = req.query.orgId;
    MongoClient.connect(`${mongoDbURL}`, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
        var dbo = db.db(`${userDbName}`);
        dbo.collection(`${registerDetails}`).find().toArray(function (err, result) {
            console.log(result)
        })
    })
    console.log(a)
    res.json(a)
}

function verifyToken(req, res, next) {
    const headerData = req.headers['authorization'];
    if (typeof headerData !== 'undefined') {
        const headerSplit = headerData.split(' ');
        const bearerToken = headerSplit[1];
        req.token = bearerToken;
        next()
    }
    else {
        res.sendStatus(403)
    }
}