const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
app.use(cors({ origin: '*' }));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE')
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next()
});
app.use(bodyParser.urlencoded({ extended: false, useUnifiedTopology: true })); // for normal data upload in body parser
// app.use(bodyParser.json({ limit: "5mb" })) // for large data parser set limit
app.use(bodyParser.json());

const urlFile = require('./common/configure'); 
// REQUIRE FILE LIST:

// sample login try
require('./login/login')(app); // login
require('./register/register')(app); // register
require('./get_details/get_user_details')(app); // get list

// Register and login user (pv-live)
require('./user-api/register')(app); // user register api
require('./user-api/login')(app); // user login api

// create user for pubnub (zq-live)
require('./notification-user/create-user')(app);

// pubnub notification (zq-live)
require('./pubnub/notification')(app);

// testing area
require('./testing/testing')(app);

// Application running port status
var instancePort = urlFile.appPortAssign;
app.listen(instancePort, () => {
    console.log(`Server is listening on port ${instancePort}`);
})
