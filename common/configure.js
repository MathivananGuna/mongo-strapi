var appPortAssign = 5000;                                          // Assign nodejs running port
var databaseURL = "mongodb://35.238.87.184:30000";                 // database url (live)
// var databaseURL = "mongodb://34.123.245.3:27017";               // database url (testing)
// var databaseURL = "mongodb://localhost:27017";                  // local database url (local)
var strapiURL = "http://34.70.214.89:1337";                        // strapi url
var pubnubSecretKey = {
    publishKey: "pub-c-22141a12-8c37-4ef6-abb2-fb4c9cffad46",      // pubnub key
    subscribeKey: "sub-c-39724d08-74e8-11eb-8687-521f348df98e",
    uuid: "sec-c-YThlNWUxNTgtM2UyOC00NzM3LTgyNjYtYWRhNTdkNzZjODY5",
}
// common login storage
var loginDatabaseName = "Accounts_login";                           // database name for login details (ALL)
var loginCollectionName = "all_logins";                             // collection name for login data (ALL)

// common storage details
var registerDetails = "register_details";                           // collection name for registered details

// User DB details
var userRegisterDbName = "user_db_register";
var userRegisterColName = "user_col_registers";

// Create-user for notification DB details
var createUserDbName = "notification_user_list";
var createUserColName = "user_lists";

// Notification DB details (pubnub) (Db is name is dynamic)
var notificationColName = "notifications"


module.exports = {
    appPortAssign,
    databaseURL,
    strapiURL,
    pubnubSecretKey,
    loginDatabaseName,
    loginCollectionName,
    registerDetails,
    userRegisterDbName,
    userRegisterColName,
    createUserDbName,
    createUserColName,
    notificationColName
}
