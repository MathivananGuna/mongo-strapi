var MongoClient = require('mongodb').MongoClient;
const mongoose = require('mongoose');
const urlFile = require('../common/configure');
const mongoDbURL = urlFile.databaseURL;
const createUserDb = urlFile.createUserDbName;
const createUserCol = urlFile.createUserColName;

const userSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, ""]
    },
    userName: {
        type: String,
        required: [true, ""]
    },
    userEmail: {
        type: String,
        required: [true, ""]
    },
    role: {
        type: String,
        required: [true, ""]
    }
})

// Database connection
const userConnection = mongoose.createConnection(`${mongoDbURL}/${createUserDb}`, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
})

module.exports = (app) => {
    app.route('/createUser')
        .get(getAllUser)
        .post(createNewuser)
}
getAllUser = (req, res) => {
    let getUserModel = userConnection.model(`${createUserCol}`, userSchema);
    getUserModel.find((error, results) => {
        res.send(results)
    })
}
createNewuser = (req, res) => { // post
    try {
        let userModel = userConnection.model(`${createUserCol}`, userSchema);
        let newUserModel = new userModel(req.body);
        newUserModel.save()
        res.send({
            status: "success", message: "registered successfully",
            data: req.body
        });
    }
    catch (err) {
        res.send({
            status: "failed", message: "register failed",
            data: req.body, message: err
        });
    }
    finally {
        const userConnection = mongoose
            .createConnection(`${mongoDbURL}/notification_user_list`, {
                useNewUrlParser: true,
                useCreateIndex: true,
                useUnifiedTopology: true,
            })
        userConnection.close()
    }
}

// DOCUMENTATION: CREATE USER

// URL: http://localhost:5000/createUser
// METHOD: POST
// PAYLOAD: {
//     "userId":"",
//     "userName":"",
//     "userEmail":"",
//     "role":""
// }
// Flow: create user for notification list



// URL: http://localhost:5000/createUser
// METHOD: GET
// Flow: check the list of created user